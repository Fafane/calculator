import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public amount: number = 0;
  title = 'calculator-interface';

  getAmount($event: number){
    this.amount = $event;
  }
}
