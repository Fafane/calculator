import { CalculatorService } from './../../../services/calculator.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReplaySubject } from 'rxjs';

import { SelectionComponent } from './selection.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('SelectionComponent', () => {
  let component: SelectionComponent;
  let fixture: ComponentFixture<SelectionComponent>;
  let fakeService;
  let snackBar;
  let calculatorSubject = new ReplaySubject(1);

  beforeEach(async () => {
    fakeService = {
      calculator: calculatorSubject.asObservable()
    }
    await TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule, FormsModule ],
      declarations: [ SelectionComponent ],
      providers: [
        { provide: CalculatorService, useValue: fakeService },
        { provide: MatSnackBar, useValue: null }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
