import { Combination } from './../../../models/combination.model';
import { CalculatorService } from './../../../services/calculator.service';
import { Component, Input, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss']
})
export class SelectionComponent implements OnInit {
  public calculatorForm!: FormGroup;
  public panelOpenState = false;
  public amountPossibleText: string = '';
  public combination: Combination | undefined;

  private errorMsg: string = 'Erreur : Veuillez contacter votre administrateur.'

  @Input() shopId: number = 0;
  @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private calculatorService: CalculatorService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.calculatorForm = this.formBuilder.group({
      amount: new FormControl(0)
    });
  }

  // Valid amount event
  onValid(): void {
    this.calculatorService.getShop(this.shopId, this.calculatorForm.value.amount).subscribe(
      result => {
        this.combination = result;
        if(result && result.equal){
          this.valueChange.emit(result.equal.value);
        } else if(result){
          const plural = result.floor && result.ceil;
          this.amountPossibleText = `Le${plural ? 's' : ''} montant${plural ? 's' : ''} possible ${plural ? 'sont' : 'est'} :`;
        }
      },
      error => {
        console.log('Erreur de communication avec le service : ' + error.message);
        this.snackBar.open(this.errorMsg, 'OK', {duration: 5000});
      }
    );
  }

  // Get next amount
  onNext(){
    this.calculatorService.getShop(this.shopId, this.calculatorForm.value.amount + 1).subscribe(
      result => {
        if(result){
          if(!result.equal){
            const plural = result.floor && result.ceil;
            this.amountPossibleText = `Le${plural ? 's' : ''} montant${plural ? 's' : ''} possible ${plural ? 'sont' : 'est'} :`;
            if(result.ceil){
              result.equal = result.ceil;
            } else {
              result.equal = result.floor;
            }
          }
          this.calculatorForm.value.amount = result.equal.value
          this.combination = result;
        }
      },
      error => {
        console.log('Erreur de communication avec le service : ' + error.message);
        this.snackBar.open(this.errorMsg, 'OK', {duration: 5000});
      }
    );
  }

  // Get previous amount
  onPrevious(){
    this.calculatorService.getShop(this.shopId, this.calculatorForm.value.amount - 1).subscribe(
      result => {
        if(result){
          if(!result.equal){
            if(result.floor){
              result.equal = result.floor;
            } else {
              result.equal = result.ceil;
            }
          }
          this.calculatorForm.value.amount = result.equal.value
          this.combination = result;
        }
      },
      error => {
        console.log('Erreur de communication avec le service : ' + error.message);
        this.snackBar.open(this.errorMsg, 'OK', {duration: 5000});
      }
    );
  }

}
