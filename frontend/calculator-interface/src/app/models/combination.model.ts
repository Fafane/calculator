//import { Amount } from './amount.model';

export class Combination {
  constructor(
    public equal: CalculatorComponentValue,
    public floor: CalculatorComponentValue,
    public ceil: CalculatorComponentValue
  ) {}
}

interface CalculatorComponentValue{
  value: number;
  cards: number[];
}
