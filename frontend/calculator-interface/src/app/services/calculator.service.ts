import { Combination } from './../models/combination.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {
  private testToken = 'tokenTest123';
  public currentValue: number | undefined;
  public combination: Combination | undefined;
  public currentValueSubject = new Subject<number>();
  public combinationSubject = new Subject<Combination>();

  constructor(private http: HttpClient) { }

  // Emit combination to all components subscribe to combinationSubject
  private emitCombinationSubject(): void{
    if(this.combination){
      this.combinationSubject.next(this.combination);
    }
  }

  // Emit current value to all components subscribe to currentValueSubject
  private emitCurrentValueSubject(): void{
    if(this.currentValue){
      this.currentValueSubject.next(this.currentValue);
    }
  }

  // Call API to get combination of amount
  public getShop(shopId: number, amount: number){
    const headers = new HttpHeaders({
        'Authorization': this.testToken
      });

    const param = new HttpParams().set('amount', amount);

    const url = `http://localhost:3000/shop/${shopId}/search-combination`;

    return this.http.get<Combination>(url, {params: param, headers: headers});
  }

  // Get combination of current amount
  public getCurrentShop(shopId: number, amount: number){
    this.getShop(shopId, amount).subscribe(
      result => {
        this.combination = result;
        this.emitCombinationSubject();
      },
      error => {
        return error;
      }
    );
  }

  // Get combination of next amount
  public getNextShop(shopId: number, amount: number){
    this.getShop(shopId, amount + 1).subscribe(
      result => {
        if(result){
          if(!result.equal){
            if(result.ceil){
              result.equal = result.ceil;
            } else {
              result.equal = result.floor;
            }
          }
          this.currentValue = result.equal.value
          this.combination = result;
          this.emitCurrentValueSubject();
          this.emitCombinationSubject();
        }
      },
      error => {
        return error;
      }
    );
  }

  // Get combination of previous amount
  public getPreviousShop(shopId: number, amount: number){
    this.getShop(shopId, amount - 1).subscribe(
      result => {
        if(result){
          if(!result.equal){
            if(result.floor){
              result.equal = result.floor;
            } else {
              result.equal = result.ceil;
            }
          }
          this.currentValue = result.equal.value
          this.combination = result;
          this.emitCurrentValueSubject();
          this.emitCombinationSubject();
        }
      }
    );
  }
}
