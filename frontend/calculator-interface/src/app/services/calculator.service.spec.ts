import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { CalculatorService } from './calculator.service';

describe('CalculatorService', () => {
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let service: CalculatorService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new CalculatorService(httpClientSpy);
  });
});
